# -*- Makefile -*-
# ----------------------------------------------
export
SHELL    = sh

INSTALLDIR = .
CHEMPROPDIR = ../tracer/chemprop

# ----------------------------------------------
### name of the executable that will be produced
# OBJECT_MODE=32 is a fix for fpc@AIX on Blizzard
FPC   = env OBJECT_MODE=32 fpc
PROG1 = imtag.exe
PROG2 = embudget.exe
CFG   = USE_PT_UPL TRACDEF_ADD_H2O $(shell test -d $(CHEMPROPDIR) && echo TRACDEF_CHEMPROP )

# --------------------------------------------------------------------
DEFOPT = -d
CDEFS = $(addprefix $(DEFOPT), $(CFG))

all: $(PROG1) $(PROG2)
	@echo "CFG: $(CFG)"

$(PROG1): imtag.pas imcom.inc imdot.inc
	@if which fpc ; then \
	  $(FPC) -l -viwnh -B $(CDEFS) imtag -o$(PROG1) ;\
	  mv -f $(PROG1) $(INSTALLDIR)/. ;\
	else \
	  echo '##########################################################';\
	  echo '### Warning: fpc (free pascal compiler) not available. ###';\
	  echo '##########################################################';\
	fi

$(PROG2): embudget.pas imcom.inc
	@if which fpc ; then \
	  $(FPC) -l -viwnh -B $(CDEFS) embudget -o$(PROG2) ;\
	  mv -f $(PROG2) $(INSTALLDIR)/. ;\
	else \
	  echo '##########################################################';\
	  echo '### Warning: fpc (free pascal compiler) not available. ###';\
	  echo '##########################################################';\
	fi

debug:
	$(FPC) -l -viwnh -B $(CDEFS) -g -gh -gl imtag -o$(PROG1) ;\
	mv -f $(PROG1) $(INSTALLDIR)/. ;\
	$(FPC) -l -viwnh -B $(CDEFS) -g -gh -gl embudget -o$(PROG2) ;\
	mv -f $(PROG2) $(INSTALLDIR)/. ;\

clean:
	rm -f *~
	rm -f *.o

distclean: clean
	rm -f $(INSTALLDIR)/$(PROG1)
	rm -f $(INSTALLDIR)/$(PROG2)

install: all

# ----------------------------------------------
