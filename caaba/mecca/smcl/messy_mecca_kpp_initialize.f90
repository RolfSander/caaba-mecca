! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! 
! Initialization File
! 
! Generated by KPP-2.2.3_rs3 symbolic chemistry Kinetics PreProcessor
!       (http://www.cs.vt.edu/~asandu/Software/KPP)
! KPP is distributed under GPL, the general public licence
!       (http://www.gnu.org/copyleft/gpl.html)
! (C) 1995-1997, V. Damian & A. Sandu, CGRER, Univ. Iowa
! (C) 1997-2005, A. Sandu, Michigan Tech, Virginia Tech
!     With important contributions from:
!        M. Damian, Villanova University, USA
!        R. Sander, Max-Planck Institute for Chemistry, Mainz, Germany
! 
! File                 : messy_mecca_kpp_Initialize.f90
! Time                 : Wed Apr 20 15:56:15 2022
! Working directory    : /home/sander/git-repository/caaba-mecca/caaba/mecca
! Equation file        : messy_mecca_kpp.kpp
! Output root filename : messy_mecca_kpp
! 
! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



MODULE messy_mecca_kpp_Initialize

  USE messy_mecca_kpp_Parameters, ONLY: dp, NVAR, NFIX
  IMPLICIT NONE

CONTAINS


! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! 
! Initialize - function to initialize concentrations
!   Arguments :
! 
! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

SUBROUTINE Initialize ( )


  USE messy_mecca_kpp_Global

  USE messy_mecca_kpp_Parameters

  INTEGER :: i
  REAL(kind=dp) :: x

  CFACTOR = 1.000000e+00_dp

  x = (0.)*CFACTOR
  DO i = 1, NVAR
    VAR(i) = x
  END DO

  x = (0.)*CFACTOR
  DO i = 1, NFIX
    FIX(i) = x
  END DO

! constant rate coefficients
  RCONST(10) = 1.8e-12
  RCONST(12) = 1
  RCONST(24) = 3.5e-12
  RCONST(37) = 1.2e-14
  RCONST(38) = 1300
  RCONST(42) = 1.66e-12
  RCONST(53) = 1.2e-12
  RCONST(63) = 3e-14
  RCONST(76) = 1.2e-12
  RCONST(82) = 1.4e-10
  RCONST(85) = 5.2e-12
  RCONST(86) = 6e-14
  RCONST(88) = 3.6e-14
  RCONST(89) = 1e-10
  RCONST(90) = 1.7e-12
  RCONST(91) = 5e-12
  RCONST(92) = 5e-12
  RCONST(93) = 5e-12
  RCONST(94) = 1e-12
  RCONST(95) = 6e-11
  RCONST(101) = 1.3e-10
  RCONST(102) = 2.53e-14
  RCONST(103) = 2.5e-11
  RCONST(104) = 4.3e-11
  RCONST(114) = 7e-15
! END constant rate coefficients

! INLINED initializations

  ! start of code from messy_mecca_kpp.kpp
  rtol(:) = 1E-2_dp ! relative tolerance
  atol(:) = 1E1_dp  ! absolute tolerance
  IF ((ind_OH  >0).AND.(ind_OH  <=NVAR)) atol(ind_OH)  = 1._dp
  IF ((ind_NO3 >0).AND.(ind_NO3 <=NVAR)) atol(ind_NO3) = 1._dp
  IF ((ind_Cl  >0).AND.(ind_Cl  <=NVAR)) atol(ind_Cl)  = 1._dp
  IF ((ind_Br  >0).AND.(ind_Br  <=NVAR)) atol(ind_Br)  = 1._dp
  IF ((ind_O1D >0).AND.(ind_O1D <=NVAR)) atol(ind_O1D) = 1._dp
  ! end of code from messy_mecca_kpp.kpp

! End INLINED initializations

      
END SUBROUTINE Initialize

! End of Initialize function
! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



END MODULE messy_mecca_kpp_Initialize

