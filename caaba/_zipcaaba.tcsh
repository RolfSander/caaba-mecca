#! /bin/tcsh -f
# Time-stamp: <2022-04-20 15:52:32 sander>
# _zipcaaba.tcsh: create a zip file of caaba code

# if the current directory is only a link, the following cd command
# will jump into the real directory:
cd `/bin/pwd`

if ( "$1" == "" ) then
  echo "This script should be called via the Makefile, e.g.:"
  echo "  gmake zip    --> archive important files"
  echo "  gmake zipall --> archive all files"
  exit
endif

set dirname = `basename $PWD`
set zipfile = $PWD/$dirname.zip
if ( -e $zipfile) then
  echo "Renaming old $dirname.zip file to $dirname.zip~"
  mv -f $zipfile $zipfile~
endif

# define links before leaving current directory:
source _internal_links.tcsh

cd ..

# zip options:
# -o make zipfile as old as latest entry
# -r recurse into directories
# -x '...' exclude files
if ( "$1" == "zipall" ) then
  zip -or $zipfile $dirname
else 
  zip -or $zipfile $dirname \
    -x '*~' -x '*.exe' -x '*.mod' -x '*.o' -x '*.pyc' -x '*.zip' \
    -x '*/__pycache__/*' -x $dirname/'caaba_*.nc' \
    -x '*/output/?*' -x '*/workdir_*/?*'
endif

# Symbolic links are now included as the whole files in the zip archive.
# However, some links are internal links (i.e. between directories that
# are both included in the zip file). They must be stored as links. This
# is done by overwriting them in the zip file.
echo "recreating internal links"
foreach link ($internal_links)
  # zip option -y to store symbolic links as the link instead of the referenced file
  zip -oy $zipfile $dirname/$link
end

if ( "$2" == "verbose" ) then
  echo;echo "The zipfile has been created:"
  ls -la $zipfile
endif

exit
